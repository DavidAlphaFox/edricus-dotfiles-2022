# edricus-dotfiles-2022
![preview](preview.png)  

## Dependencies
### Fonts
font-awesome, unifont (polybar sound/brightness bar), apple-fonts
```
# pacman -S ttf-font-awesome
$ yay -S ttf-unifont apple-fonts
```
### Apps
SDDM, Polybar, i3, Neovim, xterm, j4-menu-desktop (desktop files support for rofi), dunst, Rofi, Flameshot (screenshot tool), brightnessctl, libinput, xcompmgr (compositor)
```
# pacman -S sddm polybar i3-wm neovim xterm dunst rofi flameshot brightnessctl xorg-xinput xcompmgr qt5 qt5-quickcontrols2 qt5-svg w3m imagemagick
$ yay -S j4-dmenu-desktop
```
qt is for sddm  
w3m and imagemagick is for neofetch (image support)  

## Install
- i3/config  
Workspace control is designed to work for AZERTY keyboards  
Libinput: change id / remove commands  

- neofetch/config.ini  
It's a mess with some manually edited entries, editing is needed  

- polybar/config.ini  
Change internal/backlight card  

- themes  
Copy content to /usr/share/themes instead of ~/.themes in order to make sddm set thecursor theme for the whole desktop  

- vim  
Using Plug, install the theme with :PlugInstall  

- sddm  
Set the wallpaper in /usr/share/sddm/scripts/XSetup to avoid screen blinking when starting i3  

